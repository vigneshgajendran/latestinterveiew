package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HWDay1 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://leaftaps.com/opentaps/");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByXPath("//a[text()='Merge Leads']").click();;
		driver.findElementByXPath("(//img[(contains(@src,'/images/fieldlookup.gif'))])[1]").click();
		Set<String> win = driver.getWindowHandles();
		List<String> ls = new ArrayList<>();
		ls.addAll(win);
		
		WebDriver w1 = driver.switchTo().window(ls.get(1));
		driver.findElementByXPath("//input[@name='id']").sendKeys("10029");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
//		WebElement table = driver.findElementByXPath("//table[@class='x-grid3-row-table']");
//		List<WebElement> row = table.findElements(By.tagName("tr"));
//		WebElement row1 = row.get(1);
//		List<WebElement> cols = row1.findElements(By.tagName("tr"));
//		cols.get(0).click();
		//driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.findElementByXPath("//a[text()='10029']").click();
		
		WebDriver w0 = driver.switchTo().window(ls.get(0));		
		driver.findElementByXPath("//a[text()='10029']").click();
		win = driver.getWindowHandles();
		ls.addAll(win);
		WebDriver w2 = driver.switchTo().window(ls.get(2));
		driver.findElementByXPath("//input[@name='id']").sendKeys("10029");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']").click();
		driver.switchTo().window(ls.get(0));		
		driver.findElementByXPath("//a[@class='buttonDangerous']").click();
		driver.switchTo().alert().accept();
		
	}

}
