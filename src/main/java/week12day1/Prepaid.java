package week12day1;

public class Prepaid extends Vodafone {
	
	public void display() {
		System.out.println("Calling a method from implemented class");
		makeCall();
		System.out.println("---------------------------");
	}
	
	public void bill() {
		System.out.println("Method overriding - modifying from 'Billing' to 'Prepaid  Billing' ");
		System.out.println("Prepaid Billing");
		System.out.println("---------------------------");
	}
   
	public void calling() {
		System.out.println("Method Overloading - no aruguments");
		System.out.println("Normal Call");
		System.out.println("---------------------------");
	}

	public void calling(String p1,String p2) {
		System.out.println("Method Overloading - two aruguments");
		System.out.println("Conference Call");
		System.out.println("---------------------------");
	}
}
