package homeWork.week1;

import java.util.Scanner;

public class CodeChallenge {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CodeChallenge cc = new CodeChallenge();
//		System.out.println("Enter the month number");
		Scanner n = new Scanner(System.in);
//		int month = n.nextInt();
//		cc.monthDetails(month);

		System.out.println("Enter the number of elements array");
		int size = n.nextInt();
		int[] a = new int[size];
		System.out.println("Enter the elements in the array");
		for (int i = 0; i < a.length; i++)
			a[i] = n.nextInt();
        System.out.println("Sum is "+cc.sumOfArray(a));
	}

	public int sumOfArray(int a[]) {
		int sum = 0;
		for (int i = 0; i < a.length; i++)
			sum = sum + a[i];
		return sum;
	}

	public void monthDetails(int month) {
		switch (month) {
		case 1:
			System.out.println("January month :" + "31 days");
			break;
		case 2:
			System.out.println("February month :" + "28 days");
			break;
		case 3:
			System.out.println("March month :" + "31 days");
			break;
		case 4:
			System.out.println("April month :" + "30 days");
			break;
		case 5:
			System.out.println("May month :" + "31 days");
			break;
		case 6:
			System.out.println("June month :" + "30 days");
			break;
		case 7:
			System.out.println("July month :" + "31 days");
			break;
		case 8:
			System.out.println("August month :" + "30 days");
			break;
		case 9:
			System.out.println("September month :" + "31 days");
			break;
		case 10:
			System.out.println("October month :" + "30 days");
			break;
		case 11:
			System.out.println("November month :" + "31 days");
			break;
		case 12:
			System.out.println("December month :" + "30 days");
			break;
		}

	}

}
