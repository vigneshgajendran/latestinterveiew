package testcase;

import java.util.*;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("http://leaftaps.com/opentaps/");

		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[1]").click();
		Set<String> windowsSet = driver.getWindowHandles();
		List<String> windowsList = new ArrayList<>();
		windowsList.addAll(windowsSet);
		driver.switchTo().window(windowsList.get(1));
		driver.findElementByName("id").sendKeys("10000");
		driver.findElementByClassName("x-btn-text").click();
		driver.findElementByClassName("linktext").click();
		driver.switchTo().defaultContent();
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		windowsSet = driver.getWindowHandles();
		windowsList.addAll(windowsSet);
		driver.switchTo().window(windowsList.get(2));
		driver.findElementByName("id").sendKeys("10008");
		driver.findElementByClassName("x-btn-text").click();
		driver.findElementByClassName("linktext").click();
		driver.switchTo().defaultContent();		
		driver.findElementByClassName("buttonDangerous").click();
		driver.switchTo().alert().accept();
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		
		driver.close();

	}

}
