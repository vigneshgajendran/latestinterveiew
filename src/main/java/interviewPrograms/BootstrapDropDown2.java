package interviewPrograms;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BootstrapDropDown2 {

	public static void main(String[] args) {
		String xpath = "//div[@class='dropdown-menu' and @aria-labelledby='dropdownMenuButton']/a";
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://getbootstrap.com/docs/4.0/components/dropdowns/");
		driver.findElementByXPath("//button[@class='btn btn-secondary dropdown-toggle' and @id='dropdownMenuButton']").click();
		List<WebElement> allOption = driver.findElementsByXPath(xpath);

		for (WebElement eachOption : allOption) {
			System.out.println(eachOption.getText());
		}
	}

}
