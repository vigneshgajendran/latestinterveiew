package interviewPrograms;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BootstrapDropDown {

	public static void main(String[] args) {
		String xpath = "//li[@class='multiselect-item multiselect-group']//following::label[@class='checkbox']";
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://www.jquery-az.com/boots/demo.php?ex=63.0_2");
		driver.findElementByXPath("//button[@data-toggle='dropdown']").click();
		List<WebElement> dropdownOptions = driver.findElementsByXPath(xpath);
		for (WebElement eachOption : dropdownOptions) {
			System.out.println(eachOption.getText());
			if(eachOption.getText().contains("Java")) {
				eachOption.click();
				break;
			}
		}

	}

}
