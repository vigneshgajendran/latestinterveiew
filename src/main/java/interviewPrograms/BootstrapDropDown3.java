package interviewPrograms;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.Assert;

public class BootstrapDropDown3 {

	public static void main(String[] args) {
		//div[@class='OA0qNb ncFHed']//div[@class='MocG8c B9IrJb LMgvRb or @class='MocG8c B9IrJb LMgvRb KKjvXb
		String xpath = "//div[@class='OA0qNb ncFHed']//div[@class='MocG8c B9IrJb LMgvRb'] | div[@class='MocG8c B9IrJb LMgvRb KKjvXb']//following::content";
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(
				"https://accounts.google.com/signup/v2/webcreateaccount?continue=https%3A%2F%2Faccounts.google.com%2FManageAccount&gmb=exp&biz=false&hl=en&flowName=GlifWebSignIn&flowEntry=SignUp");
		driver.findElementByXPath("//div[@class='MocG8c B9IrJb LMgvRb KKjvXb']").click();
		List<WebElement> allOptions = driver.findElementsByXPath(xpath);
		System.out.println(allOptions.size());
		for (WebElement eachOption : allOptions) {
			System.out.println(eachOption.getText());
		}
	Assert.assertEquals(true, true);
	}

}
