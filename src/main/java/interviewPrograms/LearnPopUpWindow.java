package interviewPrograms;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnPopUpWindow {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://popuptest.com/goodpopups.html");
		String parentWindow = driver.getWindowHandle();
		System.out.println(parentWindow);
		driver.findElementByLinkText("Good PopUp #3").click();
		Set<String> windows = driver.getWindowHandles();
		System.out.println(windows);
		String childWindow = null;
		for (String eachWindow : windows) {
			if (!eachWindow.equals(parentWindow)) {
				childWindow = eachWindow;
				System.out.println(childWindow);
			}
		}
		driver.switchTo().window(childWindow);
	}
}
