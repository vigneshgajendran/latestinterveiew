package interviewPrograms;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleSearchBox {
	public static void main(String[] args) {
		String xpath = "//ul[@class='erkvQe']//following::div[@class='sbl1']";
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://www.google.com/");
		driver.findElementByXPath("//input[@role='combobox']").sendKeys("testing");

		List<WebElement> resultList = driver.findElementsByXPath(xpath);

		for (WebElement eachResult : resultList) {
			System.out.println(eachResult.getText());
			if (eachResult.getText().contains("testing tools")) {
				eachResult.click();
			}
		}
	}
}
