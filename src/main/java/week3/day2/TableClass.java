package week3.day2;

import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TableClass {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https:.erail.in");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(2000);
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS", Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("SBC", Keys.TAB);
		boolean selected = driver.findElementById("chkSelectDateOnly").isSelected();
		if (selected) {
			driver.findElementById("chkSelectDateOnly").click();
		}
		Thread.sleep(3000);
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		List<String> trainList = new ArrayList<>();
		System.out.println("----------------------------List before Sorting-------------------------");
		for (int i = 0; i < rows.size(); i++) {
			WebElement eachRow = rows.get(i);
			List<WebElement> cols = eachRow.findElements(By.tagName("td"));
			// System.out.println(cols.get(1).getText());

			trainList.add(cols.get(1).getText());
		}
		for (String eachSt : trainList)
			System.out.println(eachSt);

		driver.findElementByXPath("//a[text()='Train Name']").click();
		Thread.sleep(5000);
		WebElement newTable = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> newRows = newTable.findElements(By.tagName("tr"));
		List<String> sortedtrainList = new ArrayList<>();
		System.out.println("");
		System.out.println("---------------------------Sorted List-----------------------------");
		for (int i = 0; i < newRows.size() - 1; i++) {
			WebElement newEachRow = newRows.get(i);
			List<WebElement> newCols = newEachRow.findElements(By.tagName("td"));
			sortedtrainList.add(newCols.get(1).getText());

		}
		for (String newEachSt : sortedtrainList)
			System.out.println(newEachSt);

		// Comparing the two lists
		System.out.println("----------------------------Comparing the Lists-------------------------");
		for (String oldList : trainList) {
			if (sortedtrainList.contains(oldList)) {
				System.out.println(oldList + " is available in the sorted list");
			} else {
				System.out.println(oldList + " is not available in the sorted list");
			}
		}

	}

}