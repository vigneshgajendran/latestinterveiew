package week1.day1;

import java.util.Scanner;

public class Calc {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Calculator cal = new Calculator();

		Scanner n = new Scanner(System.in);

		System.out.println("Enter n1:");
		int a = n.nextInt();

		System.out.println("Enter n2:");
		int b = n.nextInt();
		
		System.out.println("Sum is " + cal.add(a,b));
		System.out.println("difference is " + cal.sub(a,b));
		System.out.println("quotient is " + cal.div(a,b));
		System.out.println("product is " + cal.mul(a,b));
		System.out.println("Remainder is " + cal.mod(a,b));
	}

}
