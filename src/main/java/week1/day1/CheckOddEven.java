package week1.day1;

import java.util.Scanner;

public class CheckOddEven {

	public static void main(String[] args) {
		System.out.println("Enter the number");
		Scanner n = new Scanner(System.in);
		int number = n.nextInt();
		CheckOddEven oddOrEven = new CheckOddEven();
		oddOrEven.oddOrEven(number);
	}

	public void oddOrEven(int number) {	
		if(number%2==0)
		System.out.println("Even number");
		else
		System.out.println("Odd number");
	}
		
}
