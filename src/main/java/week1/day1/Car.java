package week1.day1;

public class Car {

	public String latestModel(String model) {
		switch (model) {
		case "BMW":
			return "latest model car in BMW is BMW 7 Series";
		
		case "AUDI":
			return "latest model car in AUDI is R8 Series";
		
		case "HYUNDAI":
			return "latest model car in HYUNDAI is Creta Series";
			
		default:
			return "No data available";
		}
}
}

