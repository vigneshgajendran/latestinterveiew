package week2.day2;

import java.util.*;

public class Week2HomeWork {
	public static void main(String args[]) {

		System.out.println("Enter the starting and ending number");
		Scanner sc = new Scanner(System.in);
		int num1 = sc.nextInt();
		int num2 = sc.nextInt();
		for (int i = num1; i <= num2; i++) {
			String validate = i % 3 == 0 && i % 5 == 0 ? "FIZZBUZZ"
					: i % 3 == 0 ? "FIZZ" : i % 5 == 0 ? "BUZZ" : i + "";
			System.out.println(validate);

		}

	}
}
