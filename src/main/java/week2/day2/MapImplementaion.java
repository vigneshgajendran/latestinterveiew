package week2.day2;

import java.util.*;
import java.util.Map.*;

public class MapImplementaion {

	public static void main(String[] args) {
		Map<Integer, String> namelist = new HashMap<>();
		namelist.put(1, "one");
		namelist.put(2, "two");
		namelist.put(3, "three");
		namelist.put(4, "four");
		namelist.put(5, "five");

		for (Entry<Integer, String> itr : namelist.entrySet()) {
			System.out.println(itr.getKey() + "-->" + itr.getValue());
		}

		String text = "welcomem";
		char[] ch = text.toCharArray();

		Map<Character, Integer> cnt = new HashMap<>();

		for (int i = 0; i < ch.length; i++) {
			if (!cnt.containsKey(ch[i]))
				cnt.put(ch[i], 1);
			else
				cnt.put(ch[i], cnt.get(ch[i]) + 1);
		}

		for (Entry<Character, Integer> itr1 : cnt.entrySet())
			System.out.println(itr1.getKey() + "-->" + itr1.getValue());

		int max = 0;
		System.out.println("Maximum repeated chars are:");
		for (Entry<Character, Integer> itr1 : cnt.entrySet()) {

			if (itr1.getValue() > max) {
				max = itr1.getValue();
			}

			if (itr1.getValue() == 2)
				System.out.println(itr1.getKey());
		}
	}
}